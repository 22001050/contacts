//
//  ViewController.swift
//  contacts
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contact {
    let name: String
    let number: String
    let email: String
    let address: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var contactlist:[Contact] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as!MyCell
        let contact = contactlist[indexPath.row]
        
        cell.name.text = contact.name
        cell.number.text = contact.number
        cell.email.text = contact.email
        cell.address.text = contact.address
        
        return cell
    }
    

    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        
        contactlist.append(Contact(name: "Maria Luiza", number: "31 95689-2315", email: "marialuiza@gmail.com", address: "Rua da Água"))
        contactlist.append(Contact(name: "Luiz", number: "31 95481-4512", email: "luiz@gmail.com", address: "Rua da Pedra"))
        contactlist.append(Contact(name: "Mario", number: "31 91541-5489", email: "mario@gmail.com", address: "Rua da Terra"))
    }


}

